# DNASeq

[![pipeline status](https://gitlab.com/matkad/DNASeq/badges/master/pipeline.svg)](https://gitlab.com/matkad/DNASeq/commits/master)
[![coverage report](https://gitlab.com/matkad/DNASeq/badges/master/coverage.svg)](https://gitlab.com/matkad/DNASeq/commits/master)
[![documentation (placeholder)](https://img.shields.io/badge/docs-latest-blue.svg)](https://matkad.gitlab.io/DNASeq/)

Julia package implementing common algorithms for operating on bio-sequences.

