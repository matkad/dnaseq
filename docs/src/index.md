# DNASeq

Set of functions for most common operations on bio-sequences.
This package uses BioSequences type from BioJulia project.

```@autodocs
Modules = [DNASeq]
Order   = [:function, :type]
```
