using Documenter, DNASeq

makedocs(
    modules = [DNASeq],
    format = :html,
    checkdocs = :exports,
    sitename = "DNASeq.jl",
    pages = Any["index.md"],
    repo = "https://gitlab.com/matkad/dnaseq.jl/blob/{commit}{path}#{line}"
)
