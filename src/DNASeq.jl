module DNASeq
using BioSequences
export naive_exact_match
    """
        naive_exact_match(query::BioSequence{DNAAlphabet{4}},
                          target::BioSequence{DNAAlphabet{4}},
                          rev_comp::Bool)::Array{Int64}

    Returns the indices for the leftmost matches.
    """
    function naive_exact_match(query::BioSequence{DNAAlphabet{4}},
                               target::BioSequence{DNAAlphabet{4}},
                               rev_comp=false)
        occurrences = []
        # Iterate over alignments
        for i in 1:(length(target)-length(query))
            # Iterate over query
            match = true
            for j in 1:length(query)
                if target[i+j] != query[j]
                    match = false
                    break
                end
            end
            if match
                push!(occurrences, i)
            end
        end
        return(occurrences)
    end
end # module
