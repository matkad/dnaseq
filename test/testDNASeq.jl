using BioSequences
using Test
query = dna"ATCG"
target = dna"AATCGA"
@test naive_exact_match(query, target) == [1]
@test naive_exact_match(reverse_complement(query), reverse_complement(target)) == [1]
